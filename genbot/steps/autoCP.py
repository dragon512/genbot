from __future__ import absolute_import, division, print_function

from buildbot.plugins import *
import genbot.interfaces as interfaces
import genbot.api as api
from genbot.argument import Types, Argument
import genbot.common.is_a as is_a

from genbot.builders.genbotstep import genBotStep
import genbot.environments
import genbot.sourceinfo as sourceinfo
from buildbot.process.buildstep import (FAILURE, SUCCESS, BuildStep,
                                        BuildStepFailed, ShellMixin)
from buildbot.process.properties import Interpolate
from twisted.internet import defer
from twisted.python import log

class autoCP(interfaces.Step):
    def __init__(self, dom):
        super(autoCP, self).__init__(dom)
        self.add_arg_def([
            Argument('src_codebase', Types.String, required=True),
            Argument('dest_codebase', Types.String, required=True),
            Argument('dest_repo', Types.String, required=True),
            Argument('dest_branch', Types.String),
            Argument('dest_tracker', Types.String, required=True),
            Argument('git_name', Types.String, required=True),
            Argument('git_email', Types.String, required=True),
            Argument('git_hosts', [Types.String, Types.StringArray], required=True)
        ])

    def getObject(self):
        return autoCPSteps(**self._args)

    def getSource(self):
        return "steps.autoCP(" + self.args_as_source() + ")"

    def get_defaults(self, defaults):
        default_values = super(autoCP, self).get_defaults(defaults)
        default_values.update(
            defaults.get("autoCP", {}))

        return default_values

    def parse(self, obj, id=None):
        super(autoCP, self).parse(obj, id)


api.DefineBuildStep(autoCP)

class autoCPSteps(genBotStep):

    def __init__(self, **kw):
        self.dest_codebase = kw['dest_codebase']
        del kw['dest_codebase']
        self.dest_repo = kw['dest_repo']
        del kw['dest_repo']
        self.dest_tracker = kw['dest_tracker']
        del kw['dest_tracker']

        if 'dest_branch' in kw:
            self.dest_branch = kw['dest_branch']
            del kw['dest_branch']
        else:
            self.dest_branch = 'master'

        self.src_codebase = kw['src_codebase']
        del kw['src_codebase']

        self.git_username = kw['git_name']
        del kw['git_name']

        self.git_email = kw['git_email']
        del kw['git_email']

        self.git_hosts = kw['git_hosts']
        del kw['git_hosts']

        super(autoCPSteps, self).__init__(**kw)

    @defer.inlineCallbacks
    def run(self):

        self.source = None
        for cb in self.build.sources:
            if cb.codebase == self.src_codebase:
                self.source = cb
                break

        if not self.source:
            print("Cannot grab the correct codebase")
            defer.returnValue(FAILURE)

        self.src_repo = self.source.repository
        self.src_branch = self.source.branch
        self.revision = self.source.revision

        # print("********DUMPING********")
        # print(self.__dict__)
        # print(self.build.__dict__)
        # print("REQS")
        # print(self.build.requests[0].__dict__)
        # print([x.codebase for x in self.build.sources])
        # print([x.__dict__ for x in self.build.sources])
        # print(self.source)
        # print(self.source.__dict__)
        # print("Changes")
        # print([x.__dict__ for x in self.source.changes])
        # print(self.source.changes[0])

        # only access [0] because the git poller gets triggered for each unique commit anyway
        commit = self.source.changes[0]

        commit_comments = commit.comments.split("\n")
        commit_title = commit_comments[0]

        # needs to have 2 different cases for multiline/single line,
        # else "\n".join will make each individual character have its own new line
        if len(commit_comments) > 2:    # multiline commit message
            commit_msg = "\n".join(commit_comments[1:])
        elif len(commit_comments) == 2:  # single line commit message
            commit_msg = commit_comments[1]
        else:                           # title only
            commit_msg = ""

        add_remote = "git remote add dest {0}".format(self.src_repo)
        fetch_all = "git fetch --all"
        co_tracker = "git checkout -b {0} --track origin/{0}".format(self.dest_tracker)
        cherrypick = "git cherry-pick -x {0}".format(commit.revision)
        push = "git push"
        co_base_br = "git checkout -b {0} --track origin/{0}".format(self.dest_branch) 
        make_pr_br = "git checkout -b {0}-PR-BR {1}".format(commit.revision, self.dest_branch) 
        cp_from_tracker = "git cherry-pick -n $(git rev-list -n 1 {0})".format(self.dest_tracker)
        commit_cp = 'git commit -a -C {0}'.format(commit.revision)
        push_pr_br = "git push --set-upstream origin {0}-PR-BR".format(commit.revision)
        make_pr_msg = 'printf "Opensource cherrypick of {0}\n{1}\n(cherry picked from commit {2})" > PR_msg'.format(
            commit_title.replace('"', '\\"'), commit_msg.replace('"', '\\"'), commit.revision)
        make_pr = "~/hub pull-request -F PR_msg -b {0}".format(self.dest_branch)

        # setting up git
        setup_git = 'git config --global user.email "{0}"; git config --global user.name "{1}";'.format(self.git_email, self.git_username)
        setup_hub = "chmod +x ~/hub;"

        for host in self.git_hosts:
            setup_hub = setup_hub + ' ~/hub config --global --add hub.host {0};'.format(host)

        yield self.build.addStepsAfterCurrentStep([
            # Clone from OATS
            steps.FileDownload(
                name="Copy hub",
                mastersrc="docker_workers/hub",
                workerdest="~/hub"
            ),
            steps.ShellCommand(
                name="Change perms",
                command=setup_hub,
                workdir="",
                description="",
                haltOnFailure=True,
            ),
            steps.Git(
                repourl=self.dest_repo,
                mode='full',
                method='clobber',
                workdir='',
                codebase=self.dest_codebase,
            ),
            steps.ShellCommand(
                name="Setting up git credientials",
                command=setup_git,
                workdir="",
                description="",
                haltOnFailure=True,
            ),
            steps.ShellCommand(
                name="Adding OS-ATS Remote",
                command=add_remote,
                workdir="",
                description="",
                haltOnFailure=True,
            ),
            steps.ShellCommand(
                name="Fetch all",
                command=fetch_all,
                workdir="",
                description="",
                haltOnFailure=True,
            ),
            steps.ShellCommand(
                name="Checking out tracker branch",
                command=co_tracker,
                workdir="",
                description="",
                haltOnFailure=True,
            ),
            # should go without a hitch
            steps.ShellCommand(
                name="Cherrypicking change",
                command=cherrypick,
                workdir="",
                description="",
                haltOnFailure=True,
            ),
            steps.ShellCommand(
                name="Push the change to tracker",
                command=push,
                workdir="",
                description="",
                haltOnFailure=True,
            ),
            steps.ShellCommand(
                name="Checking out the base branch",
                command=co_base_br,
                workdir="",
                description="",
                haltOnFailure=True,
            ),
            steps.ShellCommand(
                name="Create PR branch",
                command=make_pr_br,
                workdir="",
                description="",
                haltOnFailure=True,
            ),
            # this step may result in merge conflict being commited into the branch -> fail to compile
            steps.ShellCommand(
                name="Cherrypick from tracker",
                command=cp_from_tracker,
                workdir="",
                description="",
                warnOnWarnings=True,
                warnOnFailure=True,
                flunkOnFailure=False,
            ),
            steps.ShellCommand(
                name="Add & Commit",
                command=commit_cp,
                workdir="",
                description="",
                haltOnFailure=True,
            ),
            steps.ShellCommand(
                name="Push the PR_BR",
                command=push_pr_br,
                workdir="",
                description="",
                haltOnFailure=True,
            ),
            steps.ShellCommand(
                name="Generate PR title and message file",
                command=make_pr_msg,
                workdir="",
                description="",
                haltOnFailure=True,
            ),
            # explicitly using hub instead of alias'd git
            steps.ShellCommand(
                name="Make PR",
                command=make_pr,
                workdir="",
                description="",
                haltOnFailure=True,
            ),
        ])

        defer.returnValue(SUCCESS)
