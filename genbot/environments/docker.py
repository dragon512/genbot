
from __future__ import absolute_import, division, print_function

import genbot.interfaces as interfaces
import genbot.api as api
from genbot.argument import Types, Argument

class Docker(interfaces.Environment):
    def __init__(self,dom):
        super(Docker, self).__init__(dom)
        self.add_arg_def([
            Argument('Name',Types.String,required=True),
            Argument('DockerFile',Types.String,required=True),
            Argument('DockerDir',Types.String,required=True),
        ])

        self._name = None
        self.docker_file = None
        self.docker_dir = None

    def parse(self, obj, id=None):
        if id.lower() != "docker":
            raise ValueError("Invalid Type {0}".format(obj))
        super(Docker, self).parse(obj, id)
        # set the values
        self._name = self._args["Name"]
        self.docker_file = self._args["DockerFile"]
        self.docker_dir = self._args["DockerDir"]


api.DefineEnvironment(Docker)
