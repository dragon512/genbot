from __future__ import absolute_import, division, print_function
import genbot.interfaces as interfaces
import genbot.glb as glb
import genbot.common.is_a as is_a

import genbot.environments as environments


def createEnvironment(env_info, dom):

    # this should be an dictionary with one key (which is a type)
    # it should only have 1 type
    if len(env_info) != 1:
        raise RuntimeError("Environment definition can only have one key")
    typename = list(env_info.keys())[0]
    try:
        # create the objec the type
        env = glb.known_environments[typename](dom)

    except KeyError:
        raise ValueError(
            "{typename} is not a valid value for Environment type".format(typename=typename))

    values = env.get_defaults(dom.Defaults)            
    values.update(env_info[typename]) # override defaults
    env.parse(values,typename)
    
    return env
