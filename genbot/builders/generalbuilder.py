

import os
import yaml
import pprint
import json

from twisted.python import log
from twisted.internet import defer

from buildbot.plugins import *
from buildbot.process.properties import Interpolate
from buildbot.process.buildstep import FAILURE
from buildbot.process.buildstep import SUCCESS
from buildbot.process.buildstep import BuildStep
from buildbot.process.buildstep import BuildStepFailed
from buildbot.process.buildstep import ShellMixin

from .genbotstep import genBotStep
import genbot.sourceinfo as sourceinfo
from genbot.cidom import CiDom

class GeneralBuilder(genBotStep):

    def __init__(self, **kw):
        if "name" not in kw:
            kw['name'] = 'Reading pipeline information'
        self.config = None
        genBotStep.__init__(
            self,
            haltOnFailure=True,
            flunkOnFailure=True,
            **kw)

    @defer.inlineCallbacks
    def run(self):

        self.stdio_log = yield self.addLog("Generic docker Worker\n")
        self.stdio_log.addHeader("Starting pipeline Worker\n")

        yaml_data = yield self.build.render(
            Interpolate(
                "%(prop:yaml_data)s"
            )
        )

        pipeline_name = yield self.build.render(
            Interpolate(
                "%(prop:pipeline)s"
            )
        )

        env_name = yield self.build.render(
            Interpolate(
                "%(prop:environment)s"
            )
        )
        
        raw_build_info = yield self.build.render(
            Interpolate(
                "%(prop:build_info)s"
            )
        )
        raw_build_info = json.loads(raw_build_info)
        build_info = sourceinfo.SourceInfo(**raw_build_info)

        #data = yaml.load(yaml_data)
        #yield self.stdio_log.addStdout("data: {0}\n".format(pprint.pformat(data)))
        # process it in the CiDom
        
        dom = CiDom()
        dom.load(yaml_data,build_info)

        dynsteps=dom.getDynamicSteps(pipeline_name,env_name)
        yield self.stdio_log.addStdout("Steps: {0}\n".format(pprint.pformat(dom.getStaticSteps(pipeline_name,env_name))))
        yield self.build.addStepsAfterCurrentStep(dynsteps)

        defer.returnValue(SUCCESS)

