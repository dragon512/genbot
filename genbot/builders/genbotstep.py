import os

from twisted.python import log
from twisted.internet import defer

from buildbot.plugins import *
from buildbot.process.properties import Interpolate
from buildbot.process.buildstep import FAILURE
from buildbot.process.buildstep import SUCCESS
from buildbot.process.buildstep import BuildStep
from buildbot.process.buildstep import BuildStepFailed
from buildbot.process.buildstep import ShellMixin


class genBotStep(ShellMixin, BuildStep):

    @defer.inlineCallbacks
    def CallCommand(self, name, command, **kw):
        tmp = yield self.build.render(command)
        cmd = yield self.makeRemoteShellCommand(
            stdioLogName=name,
            command=tmp,
            **kw
        )
        self.descriptionDone = "Needs to be Built!"

        yield self.runCommand(cmd)
        if cmd.didFail():
            raise BuildStepFailed()

            