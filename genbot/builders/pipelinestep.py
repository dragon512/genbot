
import json
import os
import pprint
import hashlib

import yaml

import genbot.common.string_utils as string_utils
import genbot.environments
import genbot.sourceinfo as sourceinfo
from buildbot.plugins import *
from buildbot.process.buildstep import (FAILURE, SUCCESS, BuildStep,
                                        BuildStepFailed, ShellMixin)
from buildbot.process.properties import Interpolate
from genbot.cidom import CiDom
from twisted.internet import defer
from twisted.python import log

from buildbot.util import httpclientservice

from .genbotstep import genBotStep


class PipelineBuilder(genBotStep):

    def __init__(self, **kw):
        super(PipelineBuilder, self).__init__(**kw)

    @defer.inlineCallbacks
    def run(self):

        self.stdio_log = yield self.addLog("Starting pipeline\n")
        self.stdio_log.addHeader("Basic information\n")

        raw_build_info = yield self.build.render(
            Interpolate(
                "%(prop:build_info)s"
            )
        )
        if raw_build_info:
            yield self.stdio_log.addStdout("Build information\n     {0}\n".format(pprint.pformat(raw_build_info)))
        else:
            yield self.stdio_log.addStderr("Build information repo: Not Defined\n")
            error = True
        raw_build_info = json.loads(raw_build_info)
        build_info = sourceinfo.SourceInfo(**raw_build_info)

        # Pipeline to try to build
        pipeline_name = yield self.build.render(
            Interpolate(
                "%(prop:pipeline)s"
            )
        )

        error = False
        # validate we have basic properties we need set
        if pipeline_name:
            yield self.stdio_log.addStdout("Pipeline: " + pipeline_name + "\n")
        else:
            yield self.stdio_log.addStderr("Pipeline: Not Defined\n")
            error = True

        if error:
            defer.returnValue(FAILURE)

        yield self.build.addStepsAfterCurrentStep([
            # Checkout repo with build yaml
            steps.GitHub(
                workdir="",
                repourl=build_info.repo,
                branch=build_info.branch,
                mode='full',
                method='clobber',
                codebase='buildinfo',
                shallow=20
            ),
            # copy program to make a checksum value
            steps.FileDownload(
                name="Copy checksum generator",
                mastersrc="docker_workers/dir_checksum.py",
                workerdest="dir_checksum.py",
            ),
            steps.SetPropertyFromCommand(
                command=Interpolate(
                    "cat %(prop:builddir)s/{yaml_file}".format(yaml_file=build_info.file)),
                property="yaml_data"
            ),
            # start trigger builder step
            TriggerImagePipeline(
                name="Check for Docker Images to rebuild",
                workdir="",
                schedulerNames=['dockerTrigger'],
                waitForFinish=True,
                haltOnFailure=True
            ),
            TriggerGeneralBuilder(
                name="Running pipelines",
                workdir="",
                schedulerNames=['genBuilderTrigger'],
                waitForFinish=True,
            ),
        ])
        defer.returnValue(SUCCESS)


class TriggerImagePipeline(steps.Trigger, ShellMixin):

    # def __init__(self, workers, **kw):
        # super(steps.Trigger, self).__init__(**kw)
        # super(ShellMixin, self).__init__(**kw)
        # self.workers = workers

    @defer.inlineCallbacks
    def getSchedulersAndProperties(self):

        self.stdio_log = yield self.addLog("Checking for Docker images to be built")
        self.stdio_log.addHeader("** Checking status of Docker images **\n")

        docker_registry = yield self.build.render(
            Interpolate(
                "%(prop:docker_registry)s"
            )
        )

        raw_build_info = yield self.build.render(
            Interpolate(
                "%(prop:build_info)s"
            )
        )
        raw_build_info = json.loads(raw_build_info)
        build_info = sourceinfo.SourceInfo(**raw_build_info)

        build_dir = yield self.build.render(
            Interpolate(
                "%(prop:builddir)s/"
            )
        )
        yaml_dir = os.path.split(build_info.file)[0]

        # Pipeline to try to build
        pipeline_name = yield self.build.render(
            Interpolate(
                "%(prop:pipeline)s"
            )
        )

        yaml_data = yield self.build.render(
            Interpolate(
                "%(prop:yaml_data)s"
            )
        )
        # process it in the CiDom
        dom = CiDom()
        dom.load(yaml_data, build_info)

        # validate we have a pipeline defined
        if not dom.HasPipeline(pipeline_name):
            yield self.stdio_log.addStderr("Error!: Pipeline {0} was not defined \n Found these pipelines {1}".format(pipeline_name, dom.pipelines.keys()))
            defer.returnValue(FAILURE)
        # get the Envionments for pipeline
        environments = dom.getPiplineEnvironments(pipeline_name)
        yield self.stdio_log.addStdout("Environments: {0}\n".format(environments.keys()))
        if environments is None:
            yield self.stdio_log.addStderr("Error!: Pipeline {0} has no Environments defined".format(pipeline_name))
            defer.returnValue(FAILURE)

        cmd = ''
        # generate the hash()
        for k, e in environments.items():
            if isinstance(e, genbot.environments.Docker):
                directory = os.path.normpath(
                    os.path.join(build_dir, yaml_dir, e.docker_dir)
                )
                cmd += "python dir_checksum.py -D {0};".format(directory)
        sigs = yield self.CallCommand("Getting hashes", cmd)
        sigs = sigs.split()

        self.ret = []
        for cnt, i in enumerate(environments.items()):
            k, e = i
            ############################################################
            # check that we need to build the image

            # sig = yield self.CallCommand("Get hash for {0}".format(e.name), "python dir_checksum.py -D {0}".format(directory))
            sig = sigs[cnt].split(':')[1]
            self.build.setProperty('{0}_sig'.format(e.name), sig, "pipeline")
            # check to see if registry has this?
            has_it = yield self.hasImage(
                "https://{0}".format(docker_registry),
                e.name.lower() + "_bb",
                sig
            )
            # here we generate the virtual builders
            if isinstance(e, genbot.environments.Docker) and not has_it:
                yield self.stdio_log.addStdout("Image {0}:{1} not found, Rebuilding...\n".format(e.name.lower() + "_bb", sig))
                props = self.set_properties.copy()
                props["virtual_builder_name"] = "image-{env}-{name}".format(
                    name=build_info.name,
                    env=e.name
                )
                props["virtual_builder_description"] = "Build {name} docker image".format(
                    name=e.name)
                props["virtual_builder_tags"] = [
                    "ImageBuilder", e.name.lower()
                ] + [
                    'group:' + i for i in string_utils.make_split_set(build_info.name, ".")
                ]
                props["dockerfile_dir"] = os.path.join(yaml_dir, e.docker_dir)
                props["dockerfile"] = e.docker_file
                props["dockerfile_tag"] = sig
                props["docker_image_name"] = e.name.lower()
                props["build_info"] = json.dumps(raw_build_info)
                self.ret.append(["dockerTrigger", props])
            elif has_it:
                yield self.stdio_log.addStdout("Image exists {0}:{1}, not rebuilding\n".format(e.name.lower() + "_bb", sig))
            else:
                yield self.stdio_log.addStdout("No imple to handle environment {0}: Skipping\n".format(e))

        defer.returnValue(self.ret)

    @defer.inlineCallbacks
    def CallCommand(self, name, command, **kw):
        tmp = yield self.build.render(command)
        cmd = yield self.makeRemoteShellCommand(
            stdioLogName=name,
            command=tmp,
            workdir="",
            collectStdout=True,
            **kw
        )

        yield self.runCommand(cmd)
        if cmd.didFail():
            raise BuildStepFailed()
        defer.returnValue(cmd.stdout)

    @defer.inlineCallbacks
    def hasImage(self, host, image, tag=None):
        http = yield httpclientservice.HTTPClientService.getService(
            self.master,
            host,
            verify=True)

        url = "/v2/{image}/tags/list".format(image=image)
        result = yield http.get(url)

        if result.code != 200:
            defer.returnValue(False)

        if tag:
            data = yield result.json()
            defer.returnValue(tag in data.get('tags', []))
        defer.returnValue(True)


class TriggerGeneralBuilder(steps.Trigger, ShellMixin):

    @defer.inlineCallbacks
    def getSchedulersAndProperties(self):

        self.stdio_log = yield self.addLog("Trigging build pipline")
        self.stdio_log.addHeader(
            "** Tiggering pipelibes for different Environments **\n")

        # Yaml file we are looking for to define build info
        yaml_data = yield self.build.render(
            Interpolate(
                "%(prop:yaml_data)s"
            )
        )
        # yield self.stdio_log.addStdout("Raw data: {0}\n".format(yaml_data))
        # Pipeline to try to build
        pipeline_name = yield self.build.render(
            Interpolate(
                "%(prop:pipeline)s"
            )
        )

        build_type = yield self.build.render(
            Interpolate(
                "%(prop:build_type)s"
            )
        )

        raw_build_info = yield self.build.render(
            Interpolate(
                "%(prop:build_info)s"
            )
        )
        raw_build_info = json.loads(raw_build_info)
        build_info = sourceinfo.SourceInfo(**raw_build_info)

        #data = yaml.load(yaml_data)
        # yield self.stdio_log.addStdout("data: {0}\n".format(pprint.pformat(data)))
        # process it in the CiDom
        dom = CiDom()
        dom.load(yaml_data, build_info)

        # this should be given... however we check to be safe
        # validate we have a pipeline defined
        if not dom.HasPipeline(pipeline_name):
            yield self.stdio_log.addStderr("Error!: Pipeline {0} was not defined \n Found these pipelines {1}\n".format(pipeline_name, dom.pipelines.keys()))
            defer.returnValue(FAILURE)

        build_dir = yield self.build.render(
            Interpolate(
                "%(prop:builddir)s/"
            )
        )
        yaml_dir = os.path.split(build_info.file)[0]

        # get the Envionments for pipeline
        environments = dom.getPiplineEnvironments(pipeline_name)
        self.ret = []
        for k, e in environments.items():
            # here we generate the virtual builders
            if isinstance(e, genbot.environments.Docker):
                # Get the hash()
                sig = self.build.getProperty('{0}_sig'.format(e.name))
                yield self.stdio_log.addStderr("Sig = {0}\n".format(sig))
                props = self.set_properties.copy()
                if self.build.hasProperty("Reporters"):
                    props["Reporters"] = self.build.getProperty(
                        "Reporters").copy()
                props["virtual_builder_name"] = "{pipeline}-{name}-{env}-{type}".format(
                    pipeline=pipeline_name,
                    name=build_info.name,
                    env=e.name,
                    type=build_type
                )
                props["virtual_builder_description"] = "pipline {name} build".format(
                    name=e.name)
                props["virtual_builder_tags"] = [
                    pipeline_name, e.name.lower(
                    ), "type:{0}".format(build_type)
                ] + [
                    'group:' + i for i in string_utils.make_split_set(build_info.name, ".")
                ]
                props["pipeline"] = pipeline_name
                props["build_type"] = build_type
                props["yaml_data"] = yaml_data
                props["environment"] = e.name
                props["docker_image"] = yield self.build.render(util.Interpolate('%(prop:docker_registry)s/{name}_bb:{sig}'.format(name=e.name.lower(), sig=sig)))
                props["build_info"] = json.dumps(raw_build_info)
                self.ret.append(["genBuilderTrigger", props])
            else:
                yield self.stdio_log.addStdout("No imple to handle environment {0}: Skipping\n".format(e))

        self.stdio_log.addStdout(
            "returned\n {}\n".format(pprint.pformat(self.ret)))
        defer.returnValue(self.ret)
