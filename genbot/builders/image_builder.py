
import os
import json

from twisted.python import log
from twisted.internet import defer

from buildbot.plugins import *
from buildbot.process.properties import Interpolate
from buildbot.process.buildstep import FAILURE
from buildbot.process.buildstep import SUCCESS
from buildbot.process.buildstep import BuildStep
from buildbot.process.buildstep import BuildStepFailed
from buildbot.process.buildstep import ShellMixin

from .genbotstep import genBotStep
import genbot.sourceinfo as sourceinfo


class BuildDockerImage(genBotStep):

    def __init__(self, **kw):
        if "name" not in kw:
            kw['name'] = 'Building Image'
        self.config = None
        genBotStep.__init__(
            self,
            haltOnFailure=True,
            flunkOnFailure=True,
            **kw)

    @defer.inlineCallbacks
    def run(self):

        # build base image
        build_base_cmd = Interpolate(
            "docker %(prop:docker_host:+-H %(prop:docker_host)s)s %(prop:tls_verify:+--tlsverify)s build -f %(prop:dockerfile_dir)s/%(prop:dockerfile)s -t %(prop:docker_image_name)s_base %(prop:dockerfile_dir)s")
        build_BB_cmd = Interpolate(
            "docker %(prop:docker_host:+-H %(prop:docker_host)s)s %(prop:tls_verify:+--tlsverify)s build -f dockerfile -t %(prop:docker_image_name)s_bb .")
        tag_image_to_dist_cmd_loading = Interpolate(
            "docker %(prop:docker_host:+-H %(prop:docker_host)s)s %(prop:tls_verify:+--tlsverify)s tag %(prop:docker_image_name)s_bb %(prop:docker_registry)s/%(prop:docker_image_name)s:latest")
        push_image_cmd_loading = Interpolate(
            "docker %(prop:docker_host:+-H %(prop:docker_host)s)s %(prop:tls_verify:+--tlsverify)s push %(prop:docker_registry)s/%(prop:docker_image_name)s:latest")
        tag_image_to_dist_cmd = Interpolate(
            "docker %(prop:docker_host:+-H %(prop:docker_host)s)s %(prop:tls_verify:+--tlsverify)s tag %(prop:docker_image_name)s_bb %(prop:docker_registry)s/%(prop:docker_image_name)s_bb:%(prop:dockerfile_tag)s")
        push_image_cmd = Interpolate(
            "docker %(prop:docker_host:+-H %(prop:docker_host)s)s %(prop:tls_verify:+--tlsverify)s push %(prop:docker_registry)s/%(prop:docker_image_name)s_bb:%(prop:dockerfile_tag)s")

        self.stdio_log = yield self.addLog("property dump\n")
        self.stdio_log.addHeader("porperty Dump\n")

        raw_build_info = yield self.build.render(
            Interpolate(
                "%(prop:build_info)s"
            )
        )
        raw_build_info = json.loads(raw_build_info)
        build_info = sourceinfo.SourceInfo(**raw_build_info)

        yield self.stdio_log.addStdout("Git repo = {0}\n".format(build_info.repo))

        yield self.build.addStepsAfterCurrentStep([

            steps.GitHub(
                workdir="base",
                repourl=build_info.repo,
                branch=build_info.branch,
                codebase='buildinfo',
                mode='full',
                method='clobber',
                shallow=100
            ),
            # copy the base buildbot worker .tac file
            steps.FileDownload(
                name="Copy buildbot worker definition",
                mastersrc="docker_workers/buildbot.tac",
                workerdest="buildbot.tac"
            ),
            # copy the dockerfile template to add to base image
            steps.FileDownload(
                name="Copy buildbot dockerfile overlay template",
                mastersrc="docker_workers/dockerfile.in",
                workerdest="dockerfile.in"
            ),
            steps.FileDownload(
                name="Copy gen_dockerfile.py",
                mastersrc="docker_workers/gen_dockerfile.py",
                workerdest="gen_dockerfile.py"
            ),
            steps.ShellCommand(
                name="Create base build image",
                command=build_base_cmd,
                workdir="base",
                description="Generating - Running",
                descriptionDone="Generating - Finished",
                haltOnFailure=True,
            ),
            BuildDockerImageStep(
                name="Build Docker Image",
                description="Generating - Running",
                descriptionDone="Generating - Finished",
                haltOnFailure=True,
            ),
            steps.ShellCommand(
                name="Create build CI image",
                command=build_BB_cmd,
                description="Generating - Running",
                descriptionDone="Generating - Finished",
                haltOnFailure=True,
            ),
            steps.ShellCommand(
                name="Tag image intial load into registry",
                command=tag_image_to_dist_cmd_loading,
                description="Generating - Running",
                descriptionDone="Generating - Finished",
                haltOnFailure=True,
            ),
            steps.ShellCommand(
                name="Push image into registry",
                command=push_image_cmd_loading,
                description="Generating - Running",
                descriptionDone="Generating - Finished",
                haltOnFailure=True,
            ),
            steps.ShellCommand(
                name="Tag build CI image",
                command=tag_image_to_dist_cmd,
                description="Generating - Running",
                descriptionDone="Generating - Finished",
                haltOnFailure=True,
            ),
            steps.ShellCommand(
                name="Push CI image",
                command=push_image_cmd,
                description="Generating - Running",
                descriptionDone="Generating - Finished",
                haltOnFailure=True,
            )
        ])
        defer.returnValue(SUCCESS)


class BuildDockerImageStep(genBotStep):

    @defer.inlineCallbacks
    def run(self):

        # first we need to generate the docker file based on
        # the new image we just built
        self.stdio_log = yield self.addLog("Generating dockerfile for Worker\n")
        self.stdio_log.addHeader("Generating dockerfile for Worker\n")
        yield self.stdio_log.addStdout("Reading dockerfile.in\n")

        infile_name = yield self.build.render(
            Interpolate("%(prop:builddir)s/build/dockerfile.in")
        )
        outfile_name = yield self.build.render(
            Interpolate("%(prop:builddir)s/build/dockerfile")
        )
        baseImage = yield self.build.render(
            Interpolate("%(prop:docker_image_name)s_base")
        )

        yield self.stdio_log.addStdout("Running gen_dockerfile.py\n")
        yield self.CallCommand(
            name="Running gen_dockerfile.py",
            command="python gen_dockerfile.py {0} {1} {2}".format(infile_name, outfile_name, baseImage)
        )

        defer.returnValue(SUCCESS)
