from __future__ import absolute_import, division, print_function
import genbot.interfaces as interfaces
import genbot.glb as glb
import genbot.common.is_a as is_a

import genbot.changesources

def createChangeSources(typename, env_info, dom):

    sources = []
    if typename in glb.known_cs:
        items = env_info
        for item in items:
            cs = glb.known_cs[typename](dom)
            repo = list(item.keys())[0]
            
            values = cs.get_defaults(dom.Defaults)            
            values.update(item[repo]) # override defaults
            cs.parse(values, repo)
            
            sources.append(cs)
    else:
        raise ValueError(
            "{typename} is not a valid value for Poller type".format(typename=typename))

    return sources
