from __future__ import absolute_import, division, print_function
import re

import genbot.interfaces as interfaces
import genbot.glb as glb
import genbot.common.is_a as is_a
import genbot.steps
from genbot.argument import subst_type

import genbot.common.is_a as is_a


def createStep(step_info, dom):
    if is_a.String(step_info) or subst_type(step_info):
        try:
            step = glb.known_steps[step_info](dom)
            info = {}
        except KeyError:
            step = glb.known_steps["ShellCommand"](dom)
            info = dict(command=step_info)
    else:
        if len(step_info) != 1:
            # need to figure out how to say something better to the user
            raise ValueError("Invalid Step entry",step_info)
        try:
            name = list(step_info.keys())[0]
            step = glb.known_steps[name](dom)
            info = step_info[name]
        except KeyError:
            raise ValueError("Unknown build step type: {0}".format(name))

    values = step.get_defaults(dom.Defaults)
    values.update(info)  # override defaults
    step.parse(values)
    return step


class StepEnvironments(interfaces.Base):
    def __init__(self, dom):
        super(StepEnvironments, self).__init__(dom)
        # {environment name: steps|None}
        self.overides = {}
        self.filter = None

    def parse(self, obj, id=None):
        for k, v in obj.items():
            if k.lower() in ["defaults"]:
                if is_a.Boolean(v):
                    # will use all enviuronments tags as default
                    self.filter = v
                elif is_a.String(v):
                    # some sort of reg expession to use to match with envs to use
                    self.filter = v
                else:
                    raise ValueError(
                        "{key} need to be a Boolean or String type".format(key=k))
            else:
                # this should be a predefined environment in which we want to overide steps ons
                self.parseEnvironments(k,v)

    def parseEnvironments(self, env_name, obj):

        if len(obj) != 1:
            raise RuntimeError(
                "Environment definition can only have one key, which should be the name of the environment")

        steps_data = obj.get("steps")
        steps = None
        if steps_data is not None:
            steps = []
            # We have custoem steps to use
            for step in steps_data:
                tmp = createStep(step, self.dom)
                steps.append(tmp)

        self.overides[env_name] = steps


class Pipeline(interfaces.Base):

    def __init__(self, dom):
        super(Pipeline, self).__init__(dom)
        self.environments = None
        self.steps = []

    def parse(self, obj, id=None):

        for k, v in obj.items():
            if k.lower() == "environments":
                # parse the environments
                self.parseEnvironments(v)
            elif k.lower() == "steps":
                # this is a bunch of steps
                self.parseSteps(v)
            else:
                raise ValueError(
                    "{key} is not a valid value for Pipeline".format(key=k)
                )

    def getObject(self):
        factory = util.BuildFactory()
        factory.addSteps(self.steps)
        return factory

    def parseEnvironments(self, obj):
        self.environments = StepEnvironments(self.dom)
        self.environments.parse(obj)

    def get_defaults(self, defaults):
        default_values = super(Pipeline, self).get_defaults(defaults)
        default_values.update(
            defaults.get("Pipeline", {}))
        return default_values

    def parseSteps(self, obj):
        for step in obj:
            tmp = createStep(step, self.dom)
            self.steps.append(tmp)

    def getEnvironments(self):

        ret = {}
        if self.environments is None:
            # Get all defaults
            ret = self._get_defaults_environments(True)
        else:
            environmentsFilter = self.environments.filter
            # We have some filter that is positive
            if environmentsFilter:
                ret = self._get_defaults_environments(environmentsFilter)
            
            # Add exact set that was define to any defaults
            for name in self.environments.overides.keys():
                ret[self.dom.key(name)]=self.dom.Environments[self.dom.key(name)]
                


        return ret

    def getSteps(self, env_name):
        ret = None
        # get the steps for thsi pipeline based the environment being used
        if self.environments and env_name in self.environments.overides.keys():
            # set custom steps.. else this is None
            ret = self.environments.overides[env_name]

        # use defaults if None
        if ret is None:
            ret = self.steps

        return ret

    def _get_defaults_environments(self, regexp):
        
        ret = {}
        if is_a.Boolean(regexp) and regexp:
            # Get all defaults
            for k, v in self.dom.Environments.items():
                if v.isDefault:
                    ret[k] = v
        elif is_a.String(regexp):
            filter_re = re.compile(regexp)
            for k, v in self.dom.Environments.items():
                name = v.name
                if filter_re.match(name):
                    ret[k] = v
        return ret
