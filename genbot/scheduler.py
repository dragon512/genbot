from __future__ import absolute_import, division, print_function

import genbot.glb as glb

# force load of all scheduler types to be registered
import genbot.schedulers

def createScheduler(obj_info, dom):

    # this should be an dictionary with one key (which is a type)
    # it should only have 1 type
    if len(obj_info) != 1:
        raise RuntimeError("Scheduler definition can only have one key")
    typename = list(obj_info.keys())[0]
    
    try:
        # create the objec the type
        obj = glb.known_schedulers[typename](dom)

    except KeyError:
        raise ValueError(
            "{typename} is not a valid value for Scheduler type".format(typename=typename)
            )

    values = obj.get_defaults(dom.Defaults)            
    values.update(obj_info[typename]) # override defaults
    obj.parse(values,typename)
    return obj



# need to figure out how this works in BB
'''class Tags(Schedule):
    def __init__(self):
        super(Tags, self).__init__()        

class Cron(Schedule):
    def __init__(self):
        super(Cron, self).__init__()        

class Force(Schedule):
    def __init__(self):
        super(Force, self).__init__()       '''
