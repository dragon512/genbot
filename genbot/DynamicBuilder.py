from __future__ import absolute_import, division, print_function
from buildbot.plugins import util, steps
from buildbot.process import buildstep, logobserver
from twisted.internet import defer

class DynamicBuilder(buildstep.ShellMixin, steps.BuildStep):

    def __init__(self, **kwargs):
        kwargs = self.setupShellMixin(kwargs)
        steps.BuildStep.__init__(self, **kwargs)
        self.observer = logobserver.BufferLogObserver()
        self.addLogObserver('stdio', self.observer)

    def extract_stages(self, stdout):
        # process the Yaml file
        # and return the steps

        return stages

    @defer.inlineCallbacks
    def run(self):
        # Get the yaml file data
        cmd = yield self.makeRemoteShellCommand()
        yield self.runCommand(cmd)

        # Add the steps
        result = cmd.results()
        if result == util.SUCCESS:
            new_steps=self.observer.getStdout()
            self.build.addStepsAfterCurrentStep(new_steps)
        defer.returnValue(result)


