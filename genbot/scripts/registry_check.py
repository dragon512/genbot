
from __future__ import absolute_import, division, print_function

import argparse
import requests
import pprint


class registry:
    def __init__(self, url):
        self.url = url
        if not self.url.endswith("/"):
            self.url += "/"

    def hasImage(self, image, tag=None):

        url = "{url}v2/{image}/tags/list".format(url=self.url, image=image)
        result = requests.get(url)
        if result.status_code != 200:
            return False
        if tag:
            data = result.json()
            return tag in data.get('tags', [])
        return True

    def getTags(self, image):
        url = "{url}v2/{image}/tags/list".format(url=self.url, image=image)
        result = requests.get(url)
        if result.status_code != 200:
            return []

        data = result.json()
        return data.get('tags', [])

    def getImages(self):
        url = "{url}v2/_catalog?n=9999".format(url=self.url)
        result = requests.get(url)
        if result.status_code != 200:
            print("Failed:", result.status_code)
            return False
        data = result.json()
        return data.get('repositories', [])


def main():

    parser = argparse.ArgumentParser()
    cmd_parser = parser.add_subparsers(title='Commands',
                                       dest="command")

    images = cmd_parser.add_parser(
        "images",
        help="dump all images")

    images.add_argument(
        "--registry", "-R",
        required=True,
        help="registry to query"
    )

    images.add_argument(
        "--show-tags",
        action='store_true',
        default=False,
        help="print tag info"
    )

    has_image = cmd_parser.add_parser(
        "has-image",
        help="test if image exists")

    has_image.add_argument(
        "--registry",
        required=True,
        help="registry to query"
    )

    has_image.add_argument(
        "--image", "-I",
        required=True,
        help="Image to query"
    )
    has_image.add_argument(
        "--tag", "-T",
        default=None,
        help="opional tag of image to test for"
    )

    parser.add_argument(
        '-V', '--version',
        action='version',
        version='%(prog)s 1.0'
    )

    args = parser.parse_args()

    if args.command == "images":
        myreg = registry(args.registry)
        images = myreg.getImages()
        for image in images:
            print(image)
            if args.show_tags:
                tags = myreg.getTags(image)
                print("  Tags:")
                for tag in tags:
                    print("    {0}".format(tag))

    elif args.command == "has-image":
        myreg = registry(args.registry)
        result = myreg.hasImage(args.image, args.tag)
        tag = ":{0}".format(args.tag) if args.tag is not None else ""
        if result:
            print("{image}{tag} exists!".format(image=args.image, tag=tag))
        else:
            print("{image}{tag} not found!".format(image=args.image, tag=tag))

        exit(result)


if __name__ == '__main__':
    main()
