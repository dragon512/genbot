
# file generates keys for docker clients we are doing to talk to
# can be useful for getting a set of boxs up and running quickly
# that don't have a setup already done

import argparse
import os
import subprocess

ca_seqs = [
    "openssl genrsa -aes256 -out ca.key.pem 4096",
    "openssl req -new -x509 -days {days} -key ca.key.pem -sha256 -out ca.pem"
]

server_seqs = [
    "openssl genrsa -out server.key.pem 4096",
    'openssl req -subj "/CN={host}" -sha256 -new -key server.key.pem -out server.csr',
    'echo subjectAltName = DNS:{host_short_name},DNS:{host},IP:{host_ip},IP:127.0.0.1 >> extfile.cnf',
    'echo extendedKeyUsage = serverAuth >> extfile.cnf',
    'openssl x509 -req -days 365 -sha256 -in server.csr -CA {cacert} -CAkey {cakey} -CAcreateserial -out server.cert.pem -extfile extfile.cnf',
    'cp {cacert} ./',
    'rm -f extfile.cnf server.csr'
]

client_seqs = [
    'openssl genrsa -out key.pem 4096',
    'openssl req -subj "/CN=client" -new -key key.pem -out client.csr',
    'echo extendedKeyUsage = clientAuth >> extfile.cnf',
    'openssl x509 -req -days 365 -sha256 -in client.csr -CA {cacert} -CAkey {cakey} -CAcreateserial -out cert.pem -extfile extfile.cnf',
    'cp {cacert} ./',
    'rm -f extfile.cnf client.csr',
]


def run_seq(seqs, keys={}):

    for seq in seqs:
        try:
            cmd = seq.format(**keys)
            print("\nRunning command:",cmd,"\n")
            ret = subprocess.call(cmd, shell=True)
            if ret != 0:
                print("Command failed:", cmd)
                return 1
        except KeyError:
            return 0


def gen_CA(args):
    if not os.path.exists("CA"):
        os.mkdir("CA")
    os.chdir("CA")
    ret = run_seq(ca_seqs, args)
    os.chdir("../")
    return ret


def gen_server_key_cert(args):
    dirname = "docker.server.{host_short_name}".format(**args)
    if not os.path.exists(dirname):
        os.mkdir(dirname)
    os.chdir(dirname)
    ret = run_seq(server_seqs, args)
    os.chdir("../")
    return ret


def gen_client_key_cert(args):    
    dirname = "docker.client.{host_short_name}".format(**args)
    if not os.path.exists(dirname):
        os.mkdir(dirname)
    os.chdir(dirname)
    ret = run_seq(client_seqs, args)
    os.chdir("../")
    return ret

def _path(exists, arg):
    path = os.path.abspath(arg)
    if not os.path.exists(path) and exists:
        msg = '"{0}" is not a valid path'.format(path)
        raise argparse.ArgumentTypeError(msg)
    return path

def main():
    parser = argparse.ArgumentParser()
    cmd_parser = parser.add_subparsers(title='Commands',
                                       dest="command")

    ca = cmd_parser.add_parser(
        "ca",
        help="make CA cert and key")

    ca.add_argument(
        "--days",
        default='356',
        help="The number of days CA cert file is valid"
    )

    server = cmd_parser.add_parser(
        "server",
        help="make docker server cert and key based on CA")

    client = cmd_parser.add_parser(
        "client",
        help="make docker client cert and key based on CA")

    server.add_argument(
        "--cacert",
        required=True,
        type=lambda x: _path(True,x),
        help="The CA cert file"
    )
    client.add_argument(
        "--cacert",
        required=True,
        type=lambda x: _path(True,x),
        help="The CA cert file"
    )

    server.add_argument(
        "--cakey",
        required=True,
        type=lambda x: _path(True,x),
        help="The CA key file"
    )
    client.add_argument(
        "--cakey",
        required=True,
        type=lambda x: _path(True,x),
        help="The CA key file"
    )

    server.add_argument(
        "--host",
        required=True,
        help="The DNS host name (long)"
    )
    server.add_argument(
        "--host-short-name",
        required=True,
        help="The DNS host name (short)"
    )

    server.add_argument(
        "--host-ip",
        required=True,
        help="The DNS IP value"
    )

    parser.add_argument(
        '-V', '--version',
        action='version',
        version='%(prog)s 1.0'
    )

    args = parser.parse_args()
    args_dict = vars(args)    
    if args.command == "ca":
        gen_CA(args_dict)
    elif args.command == 'server':
        gen_server_key_cert(args_dict)
        gen_client_key_cert(args_dict)
    elif args.command == 'client':
        gen_client_key_cert(args_dict)
    else:
        print("Error: Unknown type to generate keys for")


if __name__ == '__main__':
    main()
