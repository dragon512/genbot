import os
import sys

infile_name = sys.argv[1]
outfile_name = sys.argv[2]
baseImage = sys.argv[3]

print("args: in_file {0} out_file {1} baseImage {2}".format(
    infile_name, outfile_name, baseImage))

try:
    with open(infile_name) as infile:
        # read in base file
        print("before READ\n")
        out = infile.read()
        print("After READ\n")

        with open(outfile_name, 'w') as outfile:
            out = out.format(baseImage=baseImage)
            print("Writting out dockerfile\n")
            outfile.write(out)
            print("{0}\n".format(out))
    print("done")
except Exception as e:
    print("Exception: {}".format(e))
