from __future__ import absolute_import, division, print_function

import pprint
import re

import buildbot.reporters.github
from buildbot.process.properties import Interpolate, Properties
from buildbot.process.results import (CANCELLED, EXCEPTION, FAILURE, RETRY,
                                      SKIPPED, SUCCESS, WARNINGS)
from buildbot.reporters import http
from buildbot.util import httpclientservice, unicode2NativeString
from buildbot.util.giturlparse import giturlparse
from twisted.internet import defer
from twisted.python import log


class GitHubStatusPush(buildbot.reporters.github.GitHubStatusPush):

    def filterBuilds(self, build):
        return build['properties'].get("Reporters", ({},))[0].get("StatusPush", False) != False

    @defer.inlineCallbacks
    def send(self, build):
        # get the properties
        props = Properties.fromDict(build['properties'])
        self.verbose = True

        # setup core message text to set

        start = build['properties'].get("Reporters", ({},))[0].get(
            "StatusPush", {}).get('startDescription')
        end = build['properties'].get("Reporters", ({},))[0].get(
            "StatusPush", {}).get('endDescription')
        context = build['properties'].get("Reporters", ({},))[
            0].get("StatusPush", {}).get('context')

        if not start:
            start = self.startDescription
        else:
            start = Interpolate(start)
        if not end:
            end = self.endDescription
        else:
            end = Interpolate(end)
        if not context:
            context = self.context
        else:
            context = Interpolate(context)

        if build['complete']:
            state = {
                SUCCESS: 'success',
                WARNINGS: 'success',
                FAILURE: 'failure',
                SKIPPED: 'success',
                EXCEPTION: 'error',
                RETRY: 'pending',
                CANCELLED: 'error'
            }.get(build['results'], 'error')
            description = yield props.render(end)
        elif self.startDescription:
            state = 'pending'
            description = yield props.render(start)
        else:
            return
        context = yield props.render(context)
        if self.verbose:
            try:
                pipeline = props['pipeline']
            except KeyError:
                pipeline = "Unknown Pipeline"

            try:
                environment = props['environment']
            except KeyError:
                environment = "Unknown environment"
            #log.msg("Updating github status: Pipline={pipeline}, Environment={environment}".format(
                #pipeline=pipeline, environment=environment))
        # get the source stamps we need to set status on for this build
        sourcestamps = build['buildset'].get('sourcestamps')

        # quick check to make sure we have something
        if not sourcestamps or not sourcestamps[0]:
            if self.verbose:
                log.msg("Skipping github status - No sourcestamps")
            return

        # for each source stamp report out
        for sourcestamp in sourcestamps:
            sha = sourcestamp['revision']
            # must have a sha
            if sha is None:
                if self.verbose:
                    log.msg("Skipping github status - No SHA value")
                return

            project = sourcestamp['project']
            # this is useful for PRs
            try:
                branch = props['branch']
                m = re.search(r"refs/pull/([0-9]*)/merge", branch)
            except KeyError:
                m = None
            if m:
                issue = m.group(1)
            else:
                issue = None
            
            if "/" in project:
                repoOwner, repoName = project.split('/')
            else:
                giturl = giturlparse(sourcestamp['repository'])
                repoOwner = giturl.owner
                repoName = giturl.repo

            try:
                repo_user = unicode2NativeString(repoOwner)
                repo_name = unicode2NativeString(repoName)
                sha = unicode2NativeString(sha)
                state = unicode2NativeString(state)
                target_url = unicode2NativeString(build['url'])
                context = unicode2NativeString(context)
                issue = unicode2NativeString(issue)
                description = unicode2NativeString(description)

                #if self.verbose:
                    #log.msg("Updating github status: repoOwner={repoOwner}, repoName={repoName}".format(
                    #repoOwner=repoOwner, repoName=repoName))

                 #   log.msg(
                        #pprint.pformat(
                            #dict(
                                #repo_user=repo_user,
                                #repo_name=repo_name,
                                #sha=sha,
                                #state=state,
                                #target_url=target_url,
                                #context=context,
                                #issue=issue,
                                #description=description
                            #)
                        #)
                    #)

                retval = yield self.createStatus(
                    repo_user=repo_user,
                    repo_name=repo_name,
                    sha=sha,
                    state=state,
                    target_url=target_url,
                    context=context,
                    issue=issue,
                    description=description
                )
                #if self.verbose:
                    #log.msg(
                        #'Updated status with "{state}" for '
                        #'{repoOwner}/{repoName} at {sha}, issue {issue}.\n Github status value = {retval}'.format(
                            #state=state, repoOwner=repoOwner, repoName=repoName, sha=sha, issue=issue, retval=retval.original.original.code))
            except Exception as e:
                log.err(
                    e,
                    'Failed to update "{state}" for '
                    '{repoOwner}/{repoName} at {sha}, issue {issue}'.format(
                        state=state, repoOwner=repoOwner, repoName=repoName, sha=sha, issue=issue))
