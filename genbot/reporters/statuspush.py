from __future__ import absolute_import, division, print_function

import genbot.interfaces as interfaces
import genbot.api as api
import genbot.common.is_a as is_a
import genbot.common.string_utils as string_utils

from genbot.argument import Types, Argument


class StatusPush(interfaces.Reporter):
    def __init__(self, dom):
        super(StatusPush, self).__init__(dom)
        self.add_arg_def([
            Argument('startDescription', Types.String),
            Argument('endDescription', Types.String),
            Argument('context', Types.String),
        ])

    def properties(self,pipeline):
        ret = None
        
        if pipeline in self._args.get('pipelines') or self._args.get('pipelines') is None:
            keys = {'report':True}            
            if 'startDescription' in self._args:
                keys = {'startDescription' : self._args['startDescription']}
            if 'endDescription' in self._args:
                keys = {'endDescription' : self._args['endDescription']}
            if 'context' in self._args:
                keys = {'context' : self._args['context']}
            
            ret = {
            'StatusPush': keys
            }
        
        return ret


api.DefineReporter(StatusPush)
