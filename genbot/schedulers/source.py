
from __future__ import absolute_import, division, print_function

import genbot.interfaces as interfaces
import genbot.api as api
import genbot.common.is_a as is_a
import genbot.common.string_utils as string_utils

from genbot.argument import Types, Argument


class Source(interfaces.Schedule):
    def __init__(self, dom):
        super(Source, self).__init__(dom)
        self.add_arg_def([
            # change source function
            Argument('branch', [Types.String, Types.StringArray]),
            Argument('branch_re', [Types.String]),
            Argument('repository', [Types.String, Types.StringArray]),
            Argument('repository_re', [Types.String]),
            Argument('project', [Types.String, Types.StringArray]),
            Argument('project_re', [Types.String]),
            Argument('category', [Types.String]),
            # SingleSource Api
            Argument('treeStableTimer', [Types.Number])
        ])
        self.branch = None
        self.branch_re = None
        self.repository = None
        self.repository_re = None
        self.category = None
        self.category_re = None
        self.project = None
        self.project_re = None

        self.change_filter = {}
        self.codebases = []
        self.triggers = []

    def parse(self, obj, id=None):

        for key, value in obj.items():
            tmp = self.verify_args(key, value)
            if tmp:
                if tmp.ArgName in ["branch", "branch_re", "repository", "repository_re", "category", "category_re", "project", "project_re"]:
                    self.change_filter[tmp.ArgName] = value
                # we need to generate each trigger to start
                # a pipeline builder, with the builderName as
                # the pipeline property
                elif tmp.ArgName == "builderNames":
                    self.triggers += value
                elif tmp.ArgName == "codebases":
                    self.codebases += value
                else:
                    self._args[tmp.ArgName] = value
                if tmp.ArgName == "name":
                    #self._name = "{0}-{id}".format(value, id=self.buildinfo.id,)
                    self._name = value

        self._args['builderNames'] = ["PipelineStarter"]
        if not self.change_filter:
            raise ValueError("No State defined to start Trigger")

    def change_filter_source(self):

        ret = "change_filter=util.ChangeFilter("
        for k, v in self.change_filter.items():
            if is_a.String(v):
                ret += "{k}='{v}',".format(k=k, v=v)
            else:
                ret += "{k}={v},".format(k=k, v=v)
        ret += "),"
        return ret

    def get_reporter_properties(self, pipeline):
        ret = {}
        for k, v in self.dom.reporters.items():
            key = self.dom.key("", self.buildinfo)
            if key in v:
                tmp = v[key].properties(pipeline)
                # if not None ( ie we have somethig to report)
                if tmp:
                    ret.update(v[key].properties(pipeline))
        return ret

    def build_properties(self, pipeline):
        ret = "properties = dict("
        ret += '''build_info= '{info}',\n pipeline= '{pipeline}',\n virtual_builder_name='{vname}',\n virtual_builder_tags={vtags},\n build_type='{build_type}',\n'''.format(
            info=self.buildinfo.asJson(),
            pipeline=pipeline,
            vname="{bname}:{pipeline}:{sname}".format(bname=self.buildinfo.name, sname=self.name, pipeline=pipeline),
            build_type=self.name,
            vtags=['group:' + i for i in string_utils.make_split_set(self.buildinfo.name, ".")] +
            ["pipeline:" + pipeline, self.name]
        )

        tmp = self.get_reporter_properties(pipeline)
        if tmp:
            ret += '''Reporters = {0},\n'''.format(tmp)
        ret += "),"
        return ret

    def codebase(self):
        ret = "codebases ="
        # add buildinfo info
        tmp = {
            "buildinfo": {
                'repository': self.buildinfo.repo,
                'branch': self.buildinfo.branch
            }
        }
        # merge the code bases
        for base in self.codebases:
            if is_a.String(base):
                base = {base: {}}
            elif base[list(base.keys())[0]] is None:
                base[(base.keys())[0]] = {}
            tmp.update(base)
        # make it a string
        ret += str(tmp)
        ret += ",\n  "
        return ret

    def getSource(self):
        ret = ""
        for trigger in self.triggers:
            # self._name = "{0}-{id}".format(value, id=self.buildinfo.id,
            self._args["name"] = "{trigger}-{name}-{id}".format(name=self._name, trigger=trigger, id=self.buildinfo.id)
            ret += "schedulers.SingleBranchScheduler(\n    "
            ret += self.args_as_source() + "\n    "
            ret += self.change_filter_source() + "\n    "
            ret += self.build_properties(trigger) + "\n  "
            ret += self.codebase()
            ret += "),\n  "
        return ret


api.DefineScheduler(Source)
