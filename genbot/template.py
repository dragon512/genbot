from jinja2 import Template
from jinja2 import evalcontextfilter

header = Template('''
from buildbot.plugins import *
from buildbot.process.properties import Interpolate
import buildbot.util as Util

import genbot.functions as genbot


c = BuildmasterConfig = {
    'buildbotNetUsageData' : None
}

c['caches'] = {
    'Changes' : 200,
    'Builds' : 500,
    'chdicts' : 100,
    'BuildRequests' : 50,
    'SourceStamps' : 50,
    'ssdicts' : 50,
    'objectids' : 20,
    'usdicts' : 100,
}

c['protocols'] = {'pb': {'port': {{workers.port}} }}
c['builders'] = []
c['change_source'] = []
c['workers'] = []
c['schedulers'] = []

c['properties'] = {{master.properties}}

''')

code_base_generator = Template('''
server_mapper = {
    'github.com':'github',
    'bitbucket.org':'bitbucket',
{%- if master.server_mappings -%}
  {%- for domain,codebase in master.server_mappings.items() %}
    '{{domain}}':'{{codebase}}',
  {%- endfor -%}
{%- endif %}
}

def codebaseGenerator(chdict):
    repo = chdict['repository']
    repo_info=Util.giturlparse(repo)
    site=""
    for server, value in server_mapper.items():
        if server in repo_info.domain:
            site = value
    if site:
        return "{site}-{repo}".format(site=site,repo=repo_info.repo)
    return ""

c['codebaseGenerator'] = codebaseGenerator

''')


image_workers = Template('''
{% if workers.docker_image %}
# *****************************************************************************
# image workers (Builds an docker image)

image_worker_names = []
image_workers = []

{% for name,info in workers.docker_image.items() %}

{% if name in workers.hosts.keys() %}
    {%- if workers.hosts[name].type == 'docker' -%}
        {% set docker = workers.hosts[name] %}
        
image_host_{{name}} = "{{docker.host}}"
{% if docker.ca %}
import docker.tls
image_tls_{{name}} = docker.tls.TLSConfig(
    client_cert=('{{docker.client_cert}}', '{{docker.client_key}}'),
    verify= '{{docker.ca}}'
)
{% endif %}
number_image_workers_{{name}} = {{info.number}}
image_worker_names_{{name}} = [
    'image-wkr-{{name}}-{num}'.format(num=i) for i in range(0, number_image_workers_{{name}})
]
image_worker_names+=image_worker_names_{{name}}

image_workers_{{name}} = [
    worker.DockerLatentWorker(
        bot,
        "{bot}-password",
        docker_host=image_host_{{name}},
        {% if docker.volume and docker.ca %}volumes={{docker.volume}} + [
            '{{docker.client_cert}}:{% if workers.docker.home %}{{workers.docker.home}}/.docker/cert.pem'{% else %}/home/buildbot/.docker/cert.pem'{% endif %},
            '{{docker.client_key}}:{% if workers.docker.home %}{{workers.docker.home}}/.docker/key.pem'{% else %}/home/buildbot/.docker/key.pem'{% endif %},
            '{{docker.ca}}:{% if workers.docker.home %}{{workers.docker.home}}/.docker/ca.pem'{% else %}/home/buildbot/.docker/ca.pem'{% endif %}
            ],
        {% elif docker.volume %}volumes={{docker.volume}},
        {% endif %}
        image=Interpolate("%(prop:docker_registry)s/bbdockerworker"),
        followStartupLogs=True,
        masterFQDN='{{docker.master_fqdn}}',
        autopull=True,
        {% if docker.ca -%}
        tls=image_tls_{{name}},
        {% endif -%}
        max_builds=1,
        properties=dict(
            docker_host=image_host_{{name}}, 
            {% if docker.ca -%} 
            tls_verify = True
            {%- endif %}
            )
        )
    for bot in image_worker_names_{{name}}
    ]
image_workers += image_workers_{{name}}

    {% elif workers.hosts[name].type == 'LocalWorker' -%}        
number_image_workers = {{info.number}}
image_worker_names_{{name}} = [
    'image-builder-{num}'.format(num=i) for i in range(0, number_image_workers)
]

image_workers += [worker.{{workers.hosts[name].type}}(bot, max_builds=1) for bot in image_worker_names]
    {% endif %}

{%- endif -%}
{%- endfor %}
c['workers'] += image_workers
# *****************************************************************************
{% endif %}
''')

pipeline_workers = Template('''
{% if workers.pipeline %}
# *****************************************************************************
# workers for starting pipeline

pipeline_worker_names = []
pipeline_workers = []

{% for name, info in workers.pipeline.items() %}

{% if name in workers.hosts.keys() %}
    {%- if workers.hosts[name].type == 'docker' -%}
        {% set docker = workers.hosts[name] %}

pipeline_host_{{name}} = "{{docker.host}}"
{% if docker.ca %}
import docker.tls
pipeline_tls_{{name}} = docker.tls.TLSConfig(
    client_cert=('{{docker.client_cert}}', '{{docker.client_key}}'),
    verify= '{{docker.ca}}'
)
{% endif %}
number_pipeline_workers_{{name}} = {{info.number}}
pipeline_worker_names_{{name}} = [
    'pipeline-builder-{{name}}-{num}'.format(num=i) for i in range(0, number_pipeline_workers_{{name}})
]
pipeline_worker_names+=pipeline_worker_names_{{name}}

pipeline_workers_{{name}} = [
    worker.DockerLatentWorker(
        bot,
        "{bot}-password",
        docker_host=pipeline_host_{{name}},
        {% if docker.volume %}volumes={{docker.volume}},{% endif %}
        image=Interpolate("%(prop:docker_registry)s/bbdockerworker"),
        followStartupLogs=True,
        masterFQDN='{{docker.master_fqdn}}',
        autopull=True,
        {% if docker.ca -%}
        tls=pipeline_tls_{{name}},
        {% endif -%}
        max_builds=1,        
        )
    for bot in pipeline_worker_names_{{name}}
    ]
pipeline_workers += pipeline_workers_{{name}}

    {% elif workers.hosts[name].type == 'LocalWorker' -%}

number_pipeline_workers = {{info.number}}
pipeline_worker_names_{{name}} = [
    'pipeline-builder-{num}'.format(num=i) for i in range(0, number_pipeline_workers)
]

pipeline_workers += [worker.{{workers.hosts[name].type}}(bot, max_builds=1) for bot in docker_pipeline_worker_names]
    {% endif %}

{% endif %}
{%- endfor %}
c['workers'] += pipeline_workers
# *****************************************************************************
{% endif %}
''')

docker_workers = Template('''
# *****************************************************************************
# Docker workers

docker_worker_names = []
docker_workers = []

{% for name, info in workers.builders.items() %}

{% if name in workers.hosts.keys() %}
    {%- if workers.hosts[name].type == 'docker' -%}
        {% set docker = workers.hosts[name] %}

docker_host_{{name}} = "{{docker.host}}"
{% if docker.ca %}
import docker.tls
docker_tls_{{name}} = docker.tls.TLSConfig(
    client_cert=('{{docker.client_cert}}', '{{docker.client_key}}'),
    verify= '{{docker.ca}}'
)
{% endif %}
number_builder_workers_{{name}} = {{info.number}}
docker_worker_names_{{name}} = [
    'docker-bld-{{name}}-{num}'.format(num=i) for i in range(0, number_builder_workers_{{name}})
]
docker_worker_names+=docker_worker_names_{{name}}

docker_workers_{{name}} = [
    worker.DockerLatentWorker(
        bot,
        "{bot}-password",
        docker_host=docker_host_{{name}},
        {% if docker.volume %}volumes={{docker.volume}},{% endif %}
        image=Interpolate("%(prop:docker_image)s"),
        followStartupLogs=True,
        masterFQDN='{{docker.master_fqdn}}',
        autopull=True,
        {% if docker.ca -%}
        tls=docker_tls_{{name}},
        {% endif -%}
        max_builds=1        
        )
    for bot in docker_worker_names_{{name}}
    ]
docker_workers += docker_workers_{{name}}

    {%- else -%}

    # workers.hosts[name].type is a unknown or unsupported at this time

    {% endif %}
{% endif %}
{%- endfor -%}
c['workers'] += docker_workers
# *****************************************************************************

''')

# TDB
fixed_workers = Template('''
# *****************************************************************************
# workers to do the builds that cannot be in a docker image.
# *****************************************************************************

''')


db_url = Template('''
# *****************************************************************************
# DB URL
c['db'] = {
    # This specifies what database buildbot uses to store its state.  You can leave
    # this at its default for all but the largest installations.
    'db_url': "sqlite:///state.sqlite",
}
# *****************************************************************************
''')

image_trigger = Template('''
# *****************************************************************************
# trigger image builder
c['schedulers'].append(
    genbot.Triggerable(
        name="dockerTrigger",
        builderNames=["ImageBuilder"] 
    )
)
# *****************************************************************************
''')

generic_builder_trigger = Template('''
# *****************************************************************************
#trigger generic yaml builder
c['schedulers'].append(
    genbot.Triggerable(
        name="genBuilderTrigger",
        builderNames=["genBuilder"]
    )
)
# *****************************************************************************

''')

pipeline_builder = Template('''
# *****************************************************************************
pipelineBuilder = util.BuildFactory()
pipelineBuilder.addSteps([
    genbot.PipelineBuilder(),
])

c['builders'].append(
    util.BuilderConfig(
        name="PipelineStarter",
        workernames=pipeline_worker_names,
        factory=pipelineBuilder,
        # locks=[build_lock.access('counting')],
        tags=["CI-Internal"],
        collapseRequests=False
    )
)

# *****************************************************************************
''')

image_builder = Template('''
# *****************************************************************************
imageBuilder = util.BuildFactory()
imageBuilder.addSteps([
    genbot.BuildDockerImage(),
])

c['builders'].append(
    # starts up by reading yaml file
    # and making start image builders
    util.BuilderConfig(
        name="ImageBuilder",
        workernames=image_worker_names,
        factory=imageBuilder,
        # locks=[build_lock.access('counting')],
        tags=["CI-Internal"],
        collapseRequests=False
    )
)

# *****************************************************************************
''')

generic_builder = Template('''
# *****************************************************************************
genBuilder = util.BuildFactory()

genBuilder.addSteps([
    # build an image
    # and trigger it to continue building code
    genbot.GeneralBuilder(),
])

c['builders'].append(
    # starts up by reading yaml file
    # and making start image builders
    util.BuilderConfig(
        name="genBuilder",
        workernames=docker_worker_names,
        factory=genBuilder,
        # locks=[build_lock.access('counting')],
        tags=["CI-Internal"],
        collapseRequests=False
    )
)

# *****************************************************************************
''')

basic_www = Template('''
# *****************************************************************************

c['title'] = "{{master.title}}"
c['titleURL'] = "{{master.title_url}}"
c['buildbotURL'] = "{{master.title_url}}"
c['services'] = []

{% if master.reporters %}
  {%- for obj in master.reporters -%}
    {% for type, info in obj.items() %}
c['services'].append(
    genbot.{{type}}(
    {% for arg,value in info.items() -%}
    {{arg}}={%if value is string %}{%if value.startswith('Interpolate') %}{{value}}{% else %}'{{value}}'{% endif %}{% else %}{{value}}{% endif %},
    {% endfor %}  
    {%- endfor -%}
    )
)

  {% endfor %}
{% endif -%}

c['www'] = {
    'port': {{master.port}},
    'plugins': dict(
            {% for plugin in master.www.plugins %}{{plugin}}={},
            {% endfor %}),
    {% if master.www.auth -%}
    'auth': {{master.www.auth}},
    'authz': {{master.www.authz}}
    {% endif -%}
    {% if master.www.change_hook %}
    'change_hook_dialects': {
        {% for type,info in master.www.change_hook.items() -%}
        '{{type}}': {
            {% for key,value in info.items() -%}
            '{{key}}': {%if value is string %}'{{value}}'{% else %}{{value}}{% endif %},
            {% endfor -%}},
        {% endfor %}},
    {% endif -%}
    }
# *****************************************************************************
''')


#############################################################################
# this is stuff we need for image builder
#############################################################################

####################
# buildbot worker.tac
#

buildbot_tac = '''
import fnmatch
import os
import sys

from twisted.application import service
from twisted.python.log import FileLogObserver
from twisted.python.log import ILogObserver

from buildbot_worker.bot import Worker

# setup worker
basedir = os.path.abspath(os.path.dirname(__file__))
application = service.Application('buildbot-worker')


application.setComponent(ILogObserver, FileLogObserver(sys.stdout).emit)
# and worker on the same process!
buildmaster_host = os.environ.get("BUILDMASTER", 'localhost')
port = int(os.environ.get("BUILDMASTER_PORT", 9989))
workername = os.environ.get("WORKERNAME", 'docker')
passwd = os.environ.get("WORKERPASS")

# delete the password from the environ so that it is not leaked in the log
blacklist = os.environ.get("WORKER_ENVIRONMENT_BLACKLIST", "WORKERPASS").split()
for name in list(os.environ.keys()):
    for toremove in blacklist:
        if fnmatch.fnmatch(name, toremove):
            del os.environ[name]

keepalive = 600
umask = None
maxdelay = 300
allow_shutdown = None

s = Worker(buildmaster_host, port, workername, passwd, basedir,
           keepalive, umask=umask, maxdelay=maxdelay,
           allow_shutdown=allow_shutdown)
s.setServiceParent(application)
'''

####################
# worker docker file we add to base image
#

common_base = '''
# install buildbot worker
RUN pip3 install buildbot-worker --upgrade
# setup permissions
{% if workers.docker and workers.docker.setup %}
{{workers.docker.setup}}
{%- else -%}
RUN useradd -ms /bin/bash buildbot &&\
 mkdir /worker &&\
 chown -R buildbot /worker
USER buildbot
WORKDIR /worker
{% endif %}
{%- if workers.docker and workers.docker.extra -%}
{{workers.docker.extra}}
{%- endif -%}

# create build worker
COPY buildbot.tac buildbot.tac
RUN mkdir ~/.docker
# make it default action
SHELL ["/bin/bash","--login", "-c"]
ENTRYPOINT buildbot-worker start --nodaemon
'''

dockerfile = Template('''
FROM {baseImage}
''' + common_base)


############################################
# base docker image for pipelines and image builds
basic_dockerfile = Template('''
FROM ubuntu:latest

RUN apt-get update && \
	apt-get -y upgrade

RUN [ ! -e /etc/nsswitch.conf ] && echo 'hosts: files dns' > /etc/nsswitch.conf || echo "/etc/nsswitch.conf already exists"

ENV DOCKER_CHANNEL stable
ENV DOCKER_VERSION 19.03.8

RUN apt-get install -y \\
	ca-certificates \\
	curl \\
	tar \\
	build-essential \\
	git \\
	subversion \\
	python3-dev \\
	libffi-dev \\
	libssl-dev \\
	python3-pip

RUN pip3 install --upgrade\\
	buildbot-worker\\
	virtualenv

RUN set -ex; \\
	if ! curl -fL -o docker.tgz "https://download.docker.com/linux/static/${DOCKER_CHANNEL}/x86_64/docker-${DOCKER_VERSION}.tgz"; then \\
		echo >&2 "error: failed to download 'docker-${DOCKER_VERSION}' from '${DOCKER_CHANNEL}' for 'x86_64'"; \\
		exit 1; \\
	fi; \\
	\\
	tar --extract \\
		--file docker.tgz \\
		--strip-components 1 \\
		--directory /usr/local/bin/ \\
	; \\
	rm docker.tgz; \\
	dockerd -v; \\
	docker -v

COPY modprobe.sh /usr/local/bin/modprobe
COPY docker-entrypoint.sh /usr/local/bin/

''' + common_base)

gen_image = Template('''#!/bin/sh
docker build --rm -f ./Dockerfile -t bbdockerworker:latest ./
docker tag bbdockerworker:latest {{master.properties.docker_registry}}/bbdockerworker:latest
docker push {{master.properties.docker_registry}}/bbdockerworker:latest
''')

gen_image_ps = Template('''
docker build --rm -f ./Dockerfile -t bbdockerworker:latest ./
docker tag bbdockerworker:latest {{master.properties.docker_registry}}/bbdockerworker:latest
docker push {{master.properties.docker_registry}}/bbdockerworker:latest
''')
#############################################################################
# this is stuff we need for the pipeline builder
#############################################################################

####################
# this generate directory signitures
#
