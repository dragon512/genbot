from __future__ import absolute_import, division, print_function
import pprint
import yaml

from buildbot.plugins import util

import genbot.environment as environment
import genbot.pipeline as pipeline
import genbot.scheduler as scheduler
import genbot.reporter as reporter
import genbot.changesource as changesource
import genbot.common.is_a as is_a


def Interpolate_mapping(loader, node):
    return util.Interpolate(loader.construct_scalar(node))


def Properties_mapping(loader, node):
    return util.WithProperties(loader.construct_scalar(node))


class CiDom(object):

    def __init__(self):
        self.pipelines = {}
        self.Environments = {}
        self.schedulers = {}
        self.reporters = {}
        self.ChangeSources = []
        self._know_names = {}
        self._current_info = None

        # contains default args for various values
        # these can be overidden
        self.Defaults = {
            "Step": {
                'haltOnFailure': True,
            },
            "GitHub": {
                'mode': 'full',
                'method': 'clobber',
                'shallow': 50
            },
            "ChangeSource": {
                "pollAtLaunch": True,
            }

        }

    @property
    def buildinfo(self):
        return self._current_info

    def load(self, data, build_info):
        # some custom mappings
        yaml.add_constructor('!I', Interpolate_mapping)
        yaml.add_constructor('!Interpolate', Interpolate_mapping)
        yaml.add_constructor('!P', Properties_mapping)
        yaml.add_constructor('!WithProperties', Properties_mapping)
        data = yaml.load(data,Loader=yaml.FullLoader)
        self.generate(data, build_info)

    def generate(self, data, build_info):

        # process the primary objects
        self._current_info = build_info

        if "name" in data:
            v = data['name']
            if is_a.String(v):
                build_info.name = v
                if build_info.id in self._know_names:
                    print("Error!: {0} is already defined by {1}".format(v, self._know_names[v].id))
                    return

            self._know_names[v] = build_info

        else:
            print("Error!: Value must has name value defined!")
            return

        for k, v in data.items():
            if k in ["pipelines"]:
                self.parsePipelines(v)
            elif k in ["schedulers"]:
                self.parseSchedulers(v)
            elif k in ['changesource', 'cs']:
                self.parseChangeSource(v)
            elif k in ['environments']:
                self.parseEnvironments(v)
            elif k in ['reporters']:
                self.parseReporters(v)
            elif k in ['imports']:
                pass  # TBD
            elif k in ['properties']:
                pass  # TBD

        #self._current_info = None

    def key(self, key, build_info=None):

        current_info = build_info if build_info else self.buildinfo
        if current_info is None:
            raise RuntimeError("Current Build scope is not defined!")

        return "{id}:{key}".format(id=current_info.id, key=key)

    def parsePipelines(self, obj):
        for k, v in obj.items():
            p = pipeline.Pipeline(self)
            # store them so we can eaily pass them
            # down to the sub objects
            p.defaults = self.Defaults
            values = p.get_defaults(self.Defaults)
            values.update(v)  # override defaults
            p.parse(values, k)
            self.pipelines[self.key(k)] = p

    def parseChangeSource(self, obj_list):
        # this should be a list of environments
        for k, v in obj_list.items():
            tmplst = changesource.createChangeSources(k, v, self)
            self.ChangeSources += tmplst

    def parseEnvironments(self, obj_list):
        # this should be a list of environments
        for obj in obj_list:
            tmp = environment.createEnvironment(obj, self)
            self.Environments[self.key(tmp.name)] = tmp

    def parseSchedulers(self, obj_list):
        for obj in obj_list:
            tmp = scheduler.createScheduler(obj, self)
            self.schedulers[self.key(tmp.name)] = tmp

    def parseReporters(self, obj_data):
        for typename, obj in obj_data.items():
            tmp = reporter.createReporter(typename, obj, self)
            if typename in self.reporters:
                self.reporters[typename][self.key("")] = tmp
            else:
                self.reporters[typename] = {self.key(""): tmp}

    def getPiplineEnvironments(self, pipeline_name):
        pipeline = self.pipelines.get(self.key(pipeline_name))
        if pipeline:
            return pipeline.getEnvironments()
        return None

    def HasPipeline(self, name):
        if self.key(name) in self.pipelines:
            return True
        return False

    def getDynamicSteps(self, pipeline_name, env_name):

        ret = []
        # get pipeline
        pipeline = self.pipelines[self.key(pipeline_name)]
        steps = pipeline.getSteps(env_name)
        # iterate pipeline
        for step in steps:
            ret.append(step.getObject())

        return ret

    def getStaticSteps(self, pipeline_name, env_name):

        ret = []
        # get pipeline
        pipeline = self.pipelines[self.key(pipeline_name)]
        steps = pipeline.getSteps(env_name)
        # iterate pipeline
        for step in steps:
            ret.append(step.getSource())

        return ret

    def getSchedulersSource(self):

        ret = ""
        for k, v in self.schedulers.items():
            ret += "c['schedulers'] += [\n  "
            ret += v.getSource()
            ret += "]\n"

        return ret

    def getChangeSources(self):

        ret = "c['change_source'] += [\n  "
        for src in self.ChangeSources:
            ret += src.getSource()
        ret += "]"
        return ret
