from __future__ import absolute_import, division, print_function

import genbot.api as api
import genbot.common.is_a as is_a
import genbot.interfaces as interfaces
from genbot.argument import Argument, Types


class GitPoller(interfaces.ChangeSource):

    def __init__(self, dom):
        super(GitPoller, self).__init__(dom)
        self.add_arg_def([
            # change source function
            Argument('branches', [Types.Boolean, Types.StringArray]),
            Argument('name', [Types.String], required=True, arg_name='project'),
            Argument('category', [Types.String]),
            Argument(['seconds', 'pollInterval'], [Types.Number], arg_name='pollInterval')
        ])

    def parse(self, obj, id=None):

        super(GitPoller, self).parse(obj, id)
        self._args['repourl'] = id

    def getSource(self):

        ret = "changes.GitPoller("
        ret += self.args_as_source() + "\n    "
        ret += "),\n"
        return ret

api.DefineChangeSource(GitPoller, 'git-poller')
