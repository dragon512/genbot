from __future__ import absolute_import, division, print_function

import buildbot.util as Util

import genbot.api as api
import genbot.common.is_a as is_a
import genbot.interfaces as interfaces
from genbot.argument import Argument, Types


class GitHubPR(interfaces.ChangeSource):

    def __init__(self, dom):
        super(GitHubPR, self).__init__(dom)
        self.add_arg_def([
            # change source function
            #Argument('owner', [Types.String], required=True),
            #Argument('repo', [Types.String], required=True),
            Argument('branches', [Types.Boolean, Types.StringArray]),
            #Argument('name', [Types.String], required=True, arg_name='project'),
            Argument('category', [Types.String]),
            Argument('pollAtLaunch', [Types.Boolean]),
            Argument('magic_link', [Types.Boolean]),
            Argument('token', [Types.String]),
            Argument('repository_type', [Types.String]),
            Argument(['seconds', 'pollInterval'], [Types.Number], arg_name='pollInterval'),
            Argument('github_property_whitelist', [Types.StringArray]),
            Argument(['AllowedUsers','allowed_users'], [Types.StringArray],arg_name='allowed_users'),
        ])

        self._user_whitelist=None

    def parse(self, obj, id=None):

        super(GitHubPR, self).parse(obj, id)
        info = Util.giturlparse(id)
        self._args['owner'] = info.owner
        self._args['repo'] = info.repo
        
        if 'magic_link' not in self._args:
            self._args['magic_link']=True

        if 'category' not in self._args:
            self._args['category']='pull'

        if 'allowed_users' in self._args:
            self._user_whitelist = self._args['allowed_users']
            del self._args['allowed_users']             
            

    def getSource(self):

        ret = "changes.GitHubPullrequestPoller("
        ret += self.args_as_source() + "\n    "
        if self._user_whitelist:
            ret+='pullrequest_filter=genbot.PrFilter(users={users}),'.format(users=self._user_whitelist)
        ret += "),\n"
        return ret

api.DefineChangeSource(GitHubPR, 'github-pr-poller')
