from __future__ import absolute_import, division, print_function


def make_split_set(value,seperator):
    split = value.split(seperator)
    ret = []
    for size in range(0,len(split)):
        ret.append(seperator.join(split[0:size+1]))
    return ret

